import numpy as np
from constants import NODE_TOLERANCE, NODES_PER_FACE, NODES_PER_ELEMENT, FACES_PER_ELEMENT


class VertexMapping:
    """
    importantly this code distinguishes "face nodes" which lie on the face of an element, with
    "volume nodes" which exist within the element.
    """

    def __init__(self, mesh, nodal_set):
        self.mesh = mesh
        self.nodal_set = nodal_set
        self.number_of_elements = mesh.number_of_elements
        self.interior_face_nodes = None  # Vector of global nodal numbers at faces for interior values, u−
        self.exterior_face_nodes = None  # Vector of global nodal numbers at faces for exterior values, u+
        self.global_boundary_nodes = None  # corresponds to VmapB in Hestaven and Warburton
        self.face_boundary_nodes = None  # corresponds to mapB
        self.set_vector_maps()

    def set_vector_maps(self):
        # number nodes consecutively
        node_ids = np.arange(0, (self.mesh.number_of_elements * NODES_PER_ELEMENT))
        node_ids = node_ids.reshape((NODES_PER_ELEMENT, self.mesh.number_of_elements), order='F')

        # instantiate vector node maps
        interior_face_nodes = np.zeros((NODES_PER_FACE, FACES_PER_ELEMENT, self.mesh.number_of_elements)).astype(int)
        exterior_face_nodes = np.zeros((NODES_PER_FACE, FACES_PER_ELEMENT, self.mesh.number_of_elements)).astype(int)

        # reshape face_mask
        face_node_mask = self.nodal_set.standard_tetrahedron.face_node_mask.reshape(FACES_PER_ELEMENT, -1).T

        for element in range(self.mesh.number_of_elements):
            for face in range(FACES_PER_ELEMENT):
                # find index of face nodes with respect to volume node ordering
                interior_face_nodes[:, face, element] = node_ids[face_node_mask[:, face], element]

        broadcast_matrix = np.ones((1, NODES_PER_FACE))
        for element in range(self.mesh.number_of_elements):
            for face in range(FACES_PER_ELEMENT):

                # find neighbor element and adjacent face
                connected_element = self.mesh.element_to_element[element, face]
                connected_face = self.mesh.element_to_face[element, face]

                # find volume node numbers of left and right nodes
                interior_nodes = interior_face_nodes[:, face, element]
                exterior_nodes = interior_face_nodes[:, connected_face, connected_element]

                interior_x = (np.ravel(self.nodal_set.x, order='F')[interior_nodes][:, np.newaxis] @ broadcast_matrix)
                interior_y = (np.ravel(self.nodal_set.y, order='F')[interior_nodes][:, np.newaxis] @ broadcast_matrix)
                interior_z = (np.ravel(self.nodal_set.z, order='F')[interior_nodes][:, np.newaxis] @ broadcast_matrix)
                exterior_x = (np.ravel(self.nodal_set.x, order='F')[exterior_nodes][:, np.newaxis] @ broadcast_matrix)
                exterior_y = (np.ravel(self.nodal_set.y, order='F')[exterior_nodes][:, np.newaxis] @ broadcast_matrix)
                exterior_z = (np.ravel(self.nodal_set.z, order='F')[exterior_nodes][:, np.newaxis] @ broadcast_matrix)

                # compute distance matrix
                D = (interior_x - exterior_x.T) ** 2 + (interior_y - exterior_y.T) ** 2 + (interior_z - exterior_z.T) ** 2

                # find nodes that match across elements
                matched_interior_nodes, matched_exterior_nodes = np.where(np.abs(D) < NODE_TOLERANCE)
                exterior_face_nodes[matched_interior_nodes, face, element] = \
                    interior_face_nodes[matched_exterior_nodes, connected_face, connected_element]

        # unravel matrices
        self.exterior_face_nodes = exterior_face_nodes.reshape(-1, order='F')
        self.interior_face_nodes = interior_face_nodes.reshape(-1, order='F')

        # create list of boundary nodes.
        # note: if a face is on the boundary, the element_to_face matrix records a connection to itself.
        self.face_boundary_nodes = np.where(self.exterior_face_nodes == self.interior_face_nodes)[0]
        self.global_boundary_nodes = self.interior_face_nodes[self.face_boundary_nodes]

