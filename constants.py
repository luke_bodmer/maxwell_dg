import numpy as np

n = 6
dimension = 3
NODE_TOLERANCE = 1e-7

NODES_PER_ELEMENT = (n + 1) * (n + 2) * (n + 3) // 6  # no. nodes in element
NODES_PER_FACE = (n + 1) * (n + 2) // 2  # no. nodes on face
FACES_PER_ELEMENT = 4

FINAL_TIME = 10

# 4th order low storage Runge-Kutta coefficients
rk4a = np.array([0.0,
                 -567301805773.0 / 1357537059087.0,
                 -2404267990393.0 / 2016746695238.0,
                 -3550918686646.0 / 2091501179385.0,
                 -1275806237668.0 / 842570457699.0])

rk4b = np.array([1432997174477.0 / 9575080441755.0,
                 5161836677717.0 / 13612068292357.0,
                 1720146321549.0 / 2090206949498.0,
                 3134564353537.0 / 4481467310338.0,
                 2277821191437.0 / 14882151754819.0])
