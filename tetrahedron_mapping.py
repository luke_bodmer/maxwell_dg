import numpy as np
from constants import NODES_PER_FACE, FACES_PER_ELEMENT

class TetrahedronMapping:

    def __init__(self, mesh, nodal_set, element_operators):
        self.mesh = mesh
        self.nodal_set = nodal_set
        self.element_operators = element_operators
        self.face_node_mask = element_operators.standard_tetrahedron.face_node_mask
        self.rx = None
        self.ry = None
        self.rz = None
        self.sx = None
        self.sy = None
        self.sz = None
        self.tx = None
        self.ty = None
        self.tz = None
        self.nx = None
        self.ny = None
        self.nz = None
        self.surface_jacobians = None
        self.element_jacobians = None
        self.face_scale = None
        self.initialize()

    def set_geometric_factors(self):
        """Compute the metric elements for the local mappings of the elements"""

        Dr = self.element_operators.Dr
        Ds = self.element_operators.Ds
        Dt = self.element_operators.Dt

        # find jacobian of mapping
        xr = np.dot(Dr, self.nodal_set.x)
        xs = np.dot(Ds, self.nodal_set.x)
        xt = np.dot(Dt, self.nodal_set.x)
        yr = np.dot(Dr, self.nodal_set.y)
        ys = np.dot(Ds, self.nodal_set.y)
        yt = np.dot(Dt, self.nodal_set.y)
        zr = np.dot(Dr, self.nodal_set.z)
        zs = np.dot(Ds, self.nodal_set.z)
        zt = np.dot(Dt, self.nodal_set.z)

        self.element_jacobians = xr * (ys * zt - zs * yt) - yr * (xs * zt - zs * xt) + zr * (xs * yt - ys * xt)

        # compute the metric constants
        J = self.element_jacobians
        self.rx = (ys * zt - zs * yt) / J
        self.ry = -(xs * zt - zs * xt) / J
        self.rz = (xs * yt - ys * xt) / J
        self.sx = -(yr * zt - zr * yt) / J
        self.sy = (xr * zt - zr * xt) / J
        self.sz = -(xr * yt - yr * xt) / J
        self.tx = (yr * zs - zr * ys) / J
        self.ty = -(xr * zs - zr * xs) / J
        self.tz = (xr * ys - yr * xs) / J

    def set_normal_vectors(self):
        """compute outward pointing normals at elements faces as well as surface Jacobians"""

        # interpolate geometric factors to face nodes
        face_rx = self.rx[self.face_node_mask, :]
        face_sx = self.sx[self.face_node_mask, :]
        face_tx = self.tx[self.face_node_mask, :]
        face_ry = self.ry[self.face_node_mask, :]
        face_sy = self.sy[self.face_node_mask, :]
        face_ty = self.ty[self.face_node_mask, :]
        face_rz = self.rz[self.face_node_mask, :]
        face_sz = self.sz[self.face_node_mask, :]
        face_tz = self.tz[self.face_node_mask, :]

        # build normal vectors
        # the values of the nx matrix represent the x component of the normal vector at each
        # node that lies on a face of each element
        nx = np.zeros((FACES_PER_ELEMENT * NODES_PER_FACE, self.mesh.number_of_elements))
        ny = np.zeros((FACES_PER_ELEMENT * NODES_PER_FACE, self.mesh.number_of_elements))
        nz = np.zeros((FACES_PER_ELEMENT * NODES_PER_FACE, self.mesh.number_of_elements))

        # I REFACTORED EVERYTHING BELOW TO ZERO INDEX. NEED TO CHECK

        # create vectors of indices of each face
        # faces are defined in the face_node_mask of the StandardTetrahedron class.
        face1_indices = np.arange(0, NODES_PER_FACE )
        face2_indices = np.arange(NODES_PER_FACE, 2 * NODES_PER_FACE)
        face3_indices = np.arange(2 * NODES_PER_FACE, 3 * NODES_PER_FACE)
        face4_indices = np.arange(3 * NODES_PER_FACE, 4 * NODES_PER_FACE)

        # face 1
        nx[face1_indices, :] = -face_tx[face1_indices, :]
        ny[face1_indices, :] = -face_ty[face1_indices, :]
        nz[face1_indices, :] = -face_tz[face1_indices, :]

        # face 2
        nx[face2_indices, :] = -face_sx[face2_indices, :]
        ny[face2_indices, :] = -face_sy[face2_indices, :]
        nz[face2_indices, :] = -face_sz[face2_indices, :]

        # face 3
        nx[face3_indices, :] = face_rx[face3_indices, :] + face_sx[face3_indices, :] + face_tx[face3_indices, :]
        ny[face3_indices, :] = face_ry[face3_indices, :] + face_sy[face3_indices, :] + face_ty[face3_indices, :]
        nz[face3_indices, :] = face_rz[face3_indices, :] + face_sz[face3_indices, :] + face_tz[face3_indices, :]

        # face 4
        nx[face4_indices, :] = -face_rx[face4_indices, :]
        ny[face4_indices, :] = -face_ry[face4_indices, :]
        nz[face4_indices, :] = -face_rz[face4_indices, :]

        # find surface Jacobian
        surface_jacobians = np.sqrt(nx * nx + ny * ny + nz * nz)
        nx /= surface_jacobians
        ny /= surface_jacobians
        nz /= surface_jacobians
        surface_jacobians *= self.element_jacobians[self.face_node_mask, :]
        self.surface_jacobians = surface_jacobians
        self.nx = nx
        self.ny = ny
        self.nz = nz

    def set_face_scale_mapping(self):
        self.face_scale = self.surface_jacobians / self.element_jacobians[self.face_node_mask, :]

    def initialize(self):
        self.set_geometric_factors()
        self.set_normal_vectors()
        self.set_face_scale_mapping()

