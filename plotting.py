import matplotlib.pyplot as plt
from standard_tetrahedron import StandardTetrahedron
from constants import dimension, n


def plot_standard_tetrahedron():
    standard_tetrahedron = StandardTetrahedron()
    r = standard_tetrahedron.r
    s = standard_tetrahedron.s
    t = standard_tetrahedron.t

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.set_title('3D Scatter Plot of Nodes on Standard Tetrahedron')
    ax.set_xlabel('r')
    ax.set_ylabel('s')
    ax.set_zlabel('t')
    ax.scatter(r, s, t)
    plt.show()

def plot_total_energy(u, t, max_energy):
    # Plotting the y values against x values
    plt.plot(t, u)

    # Add labels and title to the plot
    plt.xlim(0, 10)
    plt.ylim(0, max_energy+10)
    plt.xlabel('time')
    plt.ylabel('Total energy')
    plt.title('Plot of E^2 + H^2 vs time')
    plt.show()
    #plt.savefig("total_energy.jpg")

