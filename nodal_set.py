from standard_tetrahedron import StandardTetrahedron
from constants import dimension, n


class NodalSet:

    def __init__(self, mesh):
        self.mesh = mesh
        self.nodal_set = None
        self.standard_tetrahedron = StandardTetrahedron()
        self.x = None
        self.y = None
        self.z = None
        self.set_xyz()

    def set_xyz(self):
        """ returns x, y, and z arrays of coordinates of nodes from EToV and VX, VY, VZ, arrays"""

        element_to_vertex = self.mesh.element_to_vertex

        r = self.standard_tetrahedron.r
        s = self.standard_tetrahedron.s
        t = self.standard_tetrahedron.t

        # extract vertex numbers from elements
        va = element_to_vertex[:, 0].T
        vb = element_to_vertex[:, 1].T
        vc = element_to_vertex[:, 2].T
        vd = element_to_vertex[:, 3].T

        VX = self.mesh.vertices_x.reshape(-1, 1)
        VY = self.mesh.vertices_y.reshape(-1, 1)
        VZ = self.mesh.vertices_z.reshape(-1, 1)

        # map r, s, t from standard tetrahedron to x, y, z coordinates for each element
        self.x = (0.5 * (-(1 + r + s + t) * VX[va] + (1 + r) * VX[vb] + (1 + s) * VX[vc] + (1 + t) * VX[vd])).T
        self.y = (0.5 * (-(1 + r + s + t) * VY[va] + (1 + r) * VY[vb] + (1 + s) * VY[vc] + (1 + t) * VY[vd])).T
        self.z = (0.5 * (-(1 + r + s + t) * VZ[va] + (1 + r) * VZ[vb] + (1 + s) * VZ[vc] + (1 + t) * VZ[vd])).T

    def initialize(self):
        self.set_xyz()