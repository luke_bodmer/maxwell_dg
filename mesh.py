"""This file defines the Mesh class. This class includes functions which can create all the necessary numpy arrays,
related to the mesh, for our  simulations. We can use this to find all connections between elements, faces,
and vertices. For example, for each element, we need to find the four elements that are connected to it. To do this,
it is helpful to consider the difference between local and global indices of elements and vertices. Each element has
a global number assigned to is (which corresponds to its position in the numpy array). Each face and vertex also have
a global index, but also has a local index (in the case of tetrahedrons, from 0 to 3). It is also important to keep
in mind the difference between vertices, which are defined by our mesh, and nodes, which our the computational
degrees of freedom that correspond to points within each element.

The numpy arrays we wish to obtain are the following:

    number_of_elements: total number of elements in the mesh [single scalar]
    number_of_vertices: total number of vertices in the mesh [single scalar]
    vertices_x: x coordinate for each vertex in the mesh [1D array]
    vertices_y: y coordinate        ""         ""         ""
    vertices_z: z coordinate        ""         ""         ""
    element_to_vertex: global vertex numbers for each element's vertices [2D: (# elements, # vertices per element)]
    element_to_element: connected element number for each face of each element [2D: (# elements, # faces per element)]
    element_to_face: connected local face number (with respect to connected element) for each face of each element
                    [2D: (# elements, # faces per element)]

Remember that if an element doesn't connect to another element (i.e. it has a face that lies on the boundary), that
element is recorded in element_to_element and element_to_face as connecting to itself. We can use this fact to find
the boundary nodes in the vertex_mappings module.
"""

import numpy as np
from constants import FACES_PER_ELEMENT


class Mesh:

    def __init__(self, filename):
        self.filename = filename
        self.number_of_elements = None
        self.number_of_vertices = None
        self.vertices_x = None
        self.vertices_y = None
        self.vertices_z = None
        self.element_to_vertex = None  # vertex numbers for each element's vertices
        self.element_to_element = None  # connected element number for each face of each element
        self.element_to_face = None  #
        self.initialize()

    def load_mesh_data(self, filename):
        """
        Read in basic grid information to build grid
        gambit *.neu format is assumed

        K: Number of elements in grid
        Nv: Number of vertices in the grid
        VX: row vector of x-coordinates for vertices in grid
        VY: row vector of x-coordinates for vertices in grid
        VZ: row vector of x-coordinates for vertices in grid
        element_to_vertex: 2D array, each row represents an element
              and the columns represent the vertex number in the element
        element_to_element: connected element number for each face of each element
        element_to_face: connected
        """

        def _skip_lines(file, n):
            # used to skip lines in the mesh file
            for i in range(n):
                file.readline()

        # open file
        f = open(filename, 'rt')

        # read intro
        _skip_lines(f, 6)

        # find number of nodes and number of elements
        dims = list(map(int, f.readline().split()))
        self.number_of_vertices = dims[0]
        self.number_of_elements = dims[1]

        # skip lines
        _skip_lines(f, 2)

        # read node coordinates
        xyz = []
        for _ in range(self.number_of_vertices):
            coordinates = list(map(float, f.readline().split()))
            xyz.append(coordinates[1:4])
        xyz = list(map(list, zip(*xyz)))

        # create VX, VY, VZ vectors
        self.vertices_x = np.array(xyz[0])
        self.vertices_y = np.array(xyz[1])
        self.vertices_z = np.array(xyz[2])

        # skip lines
        _skip_lines(f, 2)

        # read element to node connectivity
        element_to_vertex = []
        for k in range(self.number_of_elements):
            line = f.readline()
            tmp_connection = list(map(float, line.split()))
            element_to_vertex.append(tmp_connection[3:7])

        # subtract 1 to account for 0 indexing
        self.element_to_vertex = (np.array(element_to_vertex) - 1).astype(int)
        # close file
        f.close()

    def set_connectivity_matrices(self):
        # create array of all global vertices for each face, for each element, starting with face 1 of each element,
        # then 2, 3, and 4. [2D: (# elements * # faces, # vertices per face)]
        face_vertices = np.vstack((self.element_to_vertex[:, [0, 1, 2]],
                                   self.element_to_vertex[:, [0, 1, 3]],
                                   self.element_to_vertex[:, [1, 2, 3]],
                                   self.element_to_vertex[:, [0, 2, 3]]))

        # sort vertices in each row, then subtract 1. This is so [0, 2, 1] is considered the same as [ 1, 0, 2].  IT
        # SEEMS TO WORK EVEN WHEN I DON'T SUBTRACT BY 1
        face_vertices = np.sort(face_vertices, axis=1)  # - 1

        # set up default element to element and element to faces connectivity. If an element is on the boundary and
        # doesn't connect to anything, then by default, it "connects to itself". Use np.ravel to stack the matrices in
        # the same way as the face_vertices matrix.
        element_to_element = np.ravel(np.tile(np.arange(0, self.number_of_elements)[:, np.newaxis],
                                              (1, FACES_PER_ELEMENT)), order='F')
        element_to_face = np.ravel(np.tile(np.arange(0, FACES_PER_ELEMENT),
                                           (self.number_of_elements, 1)), order='F')

        # uniquely number each set of three faces by their node numbers (using formula id = (V1*Nv + V2)*Nv + V3 + 1)
        # as a very simple (perhaps too simple?) hashing function
        face_id = (face_vertices[:, 0] * (self.number_of_vertices ** 2) + face_vertices[:, 1] *
                   self.number_of_vertices + face_vertices[:, 2] + 1)

        # create array that represents the ordered indices of all faces of all elements
        ordered_face_indices = np.arange(0, FACES_PER_ELEMENT * self.number_of_elements)

        # combine id numbers, ordered indices, element to element connectivity, and element to face connectivity into a
        # single matrix. We'll use this matrix to find the indices of the matching element faces.
        master_data_array = np.column_stack((face_id,
                                             ordered_face_indices,
                                             element_to_element,
                                             element_to_face))
        # Now we have to see if any of the faces (as defined by their unique id which is a hash of their global vertex
        # numbers). First we sort by global face id.
        sorted_data_array = np.array(sorted(master_data_array, key=lambda x: (x[0])))

        # find matches in the sorted face list
        # create two sub-arrays that exclude the last, and first id number respectively, compare these results
        # to find places where there was a duplicate id
        matches = np.where(sorted_data_array[:-1, 0] == sorted_data_array[1:, 0])[0]

        # make links reflexive. If element A connects to element B, make element B also connect to element A.
        # To do this, we will create match_left and match_right matrices which contain the information from the
        # sorted_data_array for the connection in both directions
        match_left = np.vstack((sorted_data_array[matches], sorted_data_array[matches + 1]))
        match_right = np.vstack((sorted_data_array[matches + 1], sorted_data_array[matches]))

        # insert matches
        element_to_element[match_left[:, 1]] = match_right[:, 2]
        element_to_face[match_left[:, 1]] = match_right[:, 3]

        # reshape arrays
        element_to_element = element_to_element.reshape((self.number_of_elements, FACES_PER_ELEMENT), order='F')  # + 1
        element_to_face = element_to_face.reshape((self.number_of_elements, FACES_PER_ELEMENT), order='F')  # + 1

        # assign answer to class attribute
        self.element_to_element = element_to_element
        self.element_to_face = element_to_face

    def initialize(self):
        self.load_mesh_data(self.filename)
        self.set_connectivity_matrices()
