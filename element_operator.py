import numpy as np
from recursivenodes.polynomials import evaluate_vandermonde_gradient, evaluate_polynomial_vandermonde
from standard_tetrahedron import StandardTetrahedron
from constants import dimension, n, NODES_PER_ELEMENT, NODES_PER_FACE, FACES_PER_ELEMENT


class ElementOperator:

    def __init__(self):
        self.standard_tetrahedron = StandardTetrahedron()
        self.vandermonde = evaluate_polynomial_vandermonde(dimension, n, self.standard_tetrahedron.nodal_set)
        self.mass_matrix = None
        self.Dr = None  # operator that transforms point values u(r) to derivatives at these points u'(r)
        self.Ds = None
        self.Dt = None
        self.lift = None
        self.initialize()

    def set_mass_matrix(self):
        inverse_vandermonde = np.linalg.inv(self.vandermonde)
        self.mass_matrix = inverse_vandermonde.T @ inverse_vandermonde

    def set_differentiation_matrices(self):
        vandermonde_gradient = evaluate_vandermonde_gradient(dimension, n, self.standard_tetrahedron.nodal_set)

        Vr = vandermonde_gradient[:, :, 0]
        Vs = vandermonde_gradient[:, :, 1]
        Vt = vandermonde_gradient[:, :, 2]

        self.Dr = np.matmul(Vr, np.linalg.inv(self.vandermonde))
        self.Ds = np.matmul(Vs, np.linalg.inv(self.vandermonde))
        self.Dt = np.matmul(Vt, np.linalg.inv(self.vandermonde))

    def set_lift_matrix(self):
        """
        Compute 3D surface to volume lift operator used in DG formulation

        Here we only need to compute two-dimensional mass matrices because all lagrange
        polynomials where r doesn't reside on the face, are exactly zero on the face (over
        the integration domain).
        """
        r = self.standard_tetrahedron.r
        s = self.standard_tetrahedron.s
        t = self.standard_tetrahedron.t
        face_node_mask = self.standard_tetrahedron.face_node_mask

        # rearrange face_node_mask
        face_node_mask = face_node_mask.reshape(FACES_PER_ELEMENT, -1).T

        # initiate epsilon matrix
        epsilon_matrix = np.zeros((NODES_PER_ELEMENT, FACES_PER_ELEMENT * NODES_PER_FACE))

        for face in range(0, FACES_PER_ELEMENT):
            # get the nodes on the specific face
            if face == 0:
                face_r = r[face_node_mask[:, 0]]
                face_s = s[face_node_mask[:, 0]]
            elif face == 1:
                face_r = r[face_node_mask[:, 1]]
                face_s = t[face_node_mask[:, 1]]
            elif face == 2:
                face_r = s[face_node_mask[:, 2]]
                face_s = t[face_node_mask[:, 2]]
            elif face == 3:
                face_r = s[face_node_mask[:, 3]]
                face_s = t[face_node_mask[:, 3]]

            face_values = np.stack((face_r, face_s), axis=1)

            dimensions = 2
            face_vandermonde = evaluate_polynomial_vandermonde(dimensions, n, face_values)
            face_mass_matrix = np.linalg.inv(face_vandermonde @ face_vandermonde.T)

            rows_to_update = face_node_mask[:, face]
            columns_to_update = np.arange(face * NODES_PER_FACE, (face + 1) * NODES_PER_FACE)

            epsilon_matrix[rows_to_update[:, np.newaxis], columns_to_update] += face_mass_matrix

        self.lift = self.vandermonde @ (self.vandermonde.T @ epsilon_matrix)

    def initialize(self):
        self.set_mass_matrix()
        self.set_differentiation_matrices()
        self.set_lift_matrix()
