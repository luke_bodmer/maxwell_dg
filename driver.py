from mesh import Mesh
from nodal_set import NodalSet
from element_operator import ElementOperator
from tetrahedron_mapping import TetrahedronMapping
from vertex_mappings import VertexMapping
from solver import Solver
from plotting import plot_standard_tetrahedron


if __name__ == "__main__":

    # create Mesh object
    mesh = Mesh('cubeK6.neu')

    # create Nodal Set of all nodes on all elements
    nodal_set = NodalSet(mesh)

    # create all elementwise operator matrices for calculations
    element_operators = ElementOperator()

    # establish mappings from standard tetrahedron to all elements
    tetrahedron_maps = TetrahedronMapping(mesh, nodal_set, element_operators)

    #plot_standard_tetrahedron()

    vertex_maps = VertexMapping(mesh, nodal_set)

    solver = Solver(mesh, nodal_set, element_operators, tetrahedron_maps, vertex_maps)

    solver.run()
    print('done')
