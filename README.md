# Maxwell Discontinuous Galerkin Method

Maxwell_DG is a python library that implements the nodal DG method (Hestaven, Warburton  Nodal Discontinuous Galerkin 
Methods) for maxwell's electrodynamics equations.