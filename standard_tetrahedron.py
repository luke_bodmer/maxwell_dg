import numpy as np
from recursivenodes.nodes import recursive
from constants import NODE_TOLERANCE, dimension, n

class StandardTetrahedron:

    def __init__(self):
        self.nodal_set = None
        self.r = None
        self.s = None
        self.t = None
        self.face_node_mask = None
        self.set_nodal_set()
        self.set_face_node_mask()

    def set_nodal_set(self):
        nodal_set = recursive(dimension, n, domain='biunit')
        nodal_set[:, [0, 2]] = nodal_set[:, [2, 0]]
        self.nodal_set = nodal_set
        self.r = self.nodal_set[:, 0]
        self.s = self.nodal_set[:, 1]
        self.t = self.nodal_set[:, 2]

    def set_face_node_mask(self):
        """ return node indexes for nodes on each face of the standard tetrahedron"""
        face_mask1 = np.where(np.abs(1 + self.t) < NODE_TOLERANCE)[0]
        face_mask2 = np.where(np.abs(1 + self.s) < NODE_TOLERANCE)[0]
        face_mask3 = np.where(np.abs(1 + self.r + self.s + self.t) < NODE_TOLERANCE)[0]
        face_mask4 = np.where(np.abs(1 + self.r) < NODE_TOLERANCE)[0]
        self.face_node_mask = np.concatenate((face_mask1, face_mask2, face_mask3, face_mask4))
