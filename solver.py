import numpy as np
from plotting import plot_total_energy
from constants import n, rk4a, rk4b, FINAL_TIME, NODES_PER_ELEMENT, NODES_PER_FACE, FACES_PER_ELEMENT


class Solver:
    def __init__(self, mesh, nodal_set, element_operators, tetrahedron_maps, vertex_maps):
        self.mesh = mesh
        self.nodal_set = nodal_set
        self.element_operators = element_operators
        self.tetrahedron_maps = tetrahedron_maps
        self.vertex_maps = vertex_maps
        self.final_time = FINAL_TIME
        self.time = 0
        self.dt = None
        self.resHx = np.zeros((NODES_PER_ELEMENT, self.mesh.number_of_elements))
        self.resHy = np.zeros((NODES_PER_ELEMENT, self.mesh.number_of_elements))
        self.resHz = np.zeros((NODES_PER_ELEMENT, self.mesh.number_of_elements))
        self.resEx = np.zeros((NODES_PER_ELEMENT, self.mesh.number_of_elements))
        self.resEy = np.zeros((NODES_PER_ELEMENT, self.mesh.number_of_elements))
        self.resEz = np.zeros((NODES_PER_ELEMENT, self.mesh.number_of_elements))
        self.Ex = None
        self.Ey = None
        self.Ez = None
        self.Hx = None
        self.Hy = None
        self.Hz = None
        self.total_energy = []
        self.t = []

    def set_time_step_size(self):
        """
        Compute ratio between volume and face Jacobian as characteristic
        for grid to choose timestep
        """

        dt = 1.0 / (np.max(np.max(self.tetrahedron_maps.face_scale)) * n * n)

        # correct dt for integer # of time steps
        number_of_time_steps = int(np.ceil(self.final_time / dt))
        self.dt = self.final_time / number_of_time_steps

    def set_initial_conditions(self):
        self.Hx = np.zeros((NODES_PER_ELEMENT, self.mesh.number_of_elements))
        self.Hy = np.zeros((NODES_PER_ELEMENT, self.mesh.number_of_elements))
        self.Hz = np.zeros((NODES_PER_ELEMENT, self.mesh.number_of_elements))
        self.Ex = np.zeros((NODES_PER_ELEMENT, self.mesh.number_of_elements))
        self.Ey = np.zeros((NODES_PER_ELEMENT, self.mesh.number_of_elements))
        xmode = 1
        ymode = 1
        self.Ez = np.sin(xmode * np.pi * self.nodal_set.x) * np.sin(ymode * np.pi * self.nodal_set.y)

    def curl(self, Ux, Uy, Uz):
        """compute local elemental physical spatial curl of (Ux, Uy, Uz)"""

        Dr = self.element_operators.Dr
        Ds = self.element_operators.Ds
        Dt = self.element_operators.Dt

        rx = self.tetrahedron_maps.rx
        sx = self.tetrahedron_maps.sx
        tx = self.tetrahedron_maps.tx
        ry = self.tetrahedron_maps.ry
        sy = self.tetrahedron_maps.sy
        ty = self.tetrahedron_maps.ty
        rz = self.tetrahedron_maps.rz
        sz = self.tetrahedron_maps.sz
        tz = self.tetrahedron_maps.tz

        # compute local derivatives of Ux on reference tetrahedron
        ddr = Dr @ Ux
        dds = Ds @ Ux
        ddt = Dt @ Ux

        # increment curl components
        curl_y = rz * ddr + sz * dds + tz * ddt
        curl_z = -(ry * ddr + sy * dds + ty * ddt)

        # compute local derivatives of Uy on reference tetrahedron
        ddr = Dr @ Uy
        dds = Ds @ Uy
        ddt = Dt @ Uy

        # increment curl components
        curl_x = -(rz * ddr + sz * dds + tz * ddt)
        curl_z += rx * ddr + sx * dds + tx * ddt

        # compute local derivatives of Uz on reference tetrahedron
        ddr = Dr @ Uz
        dds = Ds @ Uz
        ddt = Dt @ Uz

        # increment curl components
        curl_x += ry * ddr + sy * dds + ty * ddt
        curl_y -= rx * ddr + sx * dds + tx * ddt

        return curl_x, curl_y, curl_z

    def increment_time(self):
        for i in range(0, 5):  # inner multi-stage Runge-Kutta loop
            # compute right hand side of TM-mode Maxwell's equations
            rhsHx, rhsHy, rhsHz, rhsEx, rhsEy, rhsEz = self.compute_rhs()

            # initiate, increment Runge-Kutta residuals and update fields
            self.resHx = rk4a[i] * self.resHx + self.dt * rhsHx
            self.Hx = self.Hx + rk4b[i] * self.resHx
            self.resHy = rk4a[i] * self.resHy + self.dt * rhsHy
            self.Hy = self.Hy + rk4b[i] * self.resHy
            self.resHz = rk4a[i] * self.resHz + self.dt * rhsHz
            self.Hz = self.Hz + rk4b[i] * self.resHz
            self.resEx = rk4a[i] * self.resEx + self.dt * rhsEx
            self.Ex = self.Ex + rk4b[i] * self.resEx
            self.resEy = rk4a[i] * self.resEy + self.dt * rhsEy
            self.Ey = self.Ey + rk4b[i] * self.resEy
            self.resEz = rk4a[i] * self.resEz + self.dt * rhsEz
            self.Ez = self.Ez + rk4b[i] * self.resEz

        self.time += self.dt  # Increment time

        # log the current timestep to the terminal
        current_step = int(self.time // self.dt)
        total_steps = int(FINAL_TIME/self.dt)
        print(f"{current_step} / {total_steps}")

        H_mag = self.Hx**2 + self.Hy**2 + self.Hz**2
        E_mag = self.Ex**2 + self.Ey**2 + self.Ez**2
        total_energy = np.sum(H_mag) + np.sum(E_mag)
        self.total_energy.append(total_energy)
        self.t.append(self.time)

    def compute_rhs(self):

        # vector mappings
        interior_face_nodes = self.vertex_maps.interior_face_nodes
        exterior_face_nodes = self.vertex_maps.exterior_face_nodes  # interior values of u
        global_boundary_nodes = self.vertex_maps.global_boundary_nodes  # interior boundary values
        face_boundary_nodes = self.vertex_maps.face_boundary_nodes

        # normal vectors
        nx = self.tetrahedron_maps.nx
        ny = self.tetrahedron_maps.ny
        nz = self.tetrahedron_maps.nz

        # calculate the change in values across element faces
        dHx = np.ravel(self.Hx, order='F')[exterior_face_nodes] - np.ravel(self.Hx, order='F')[interior_face_nodes]
        dHy = np.ravel(self.Hy, order='F')[exterior_face_nodes] - np.ravel(self.Hy, order='F')[interior_face_nodes]
        dHz = np.ravel(self.Hz, order='F')[exterior_face_nodes] - np.ravel(self.Hz, order='F')[interior_face_nodes]
        dEx = np.ravel(self.Ex, order='F')[exterior_face_nodes] - np.ravel(self.Ex, order='F')[interior_face_nodes]
        dEy = np.ravel(self.Ey, order='F')[exterior_face_nodes] - np.ravel(self.Ey, order='F')[interior_face_nodes]
        dEz = np.ravel(self.Ez, order='F')[exterior_face_nodes] - np.ravel(self.Ez, order='F')[interior_face_nodes]

        self.Ex = np.ravel(self.Ex, order='F')
        self.Ey = np.ravel(self.Ey, order='F')
        self.Ez = np.ravel(self.Ez, order='F')

        # make boundary conditions all reflective (Ez+ = -Ez-)
        dHx[face_boundary_nodes] = 0
        dEx[face_boundary_nodes] = -2 * self.Ex[global_boundary_nodes]
        dHy[face_boundary_nodes] = 0
        dEy[face_boundary_nodes] = -2 * self.Ey[global_boundary_nodes]
        dHz[face_boundary_nodes] = 0
        dEz[face_boundary_nodes] = -2 * self.Ez[global_boundary_nodes]

        dHx = dHx.reshape((NODES_PER_FACE * FACES_PER_ELEMENT, self.mesh.number_of_elements), order='F')
        dHy = dHy.reshape((NODES_PER_FACE * FACES_PER_ELEMENT, self.mesh.number_of_elements), order='F')
        dHz = dHz.reshape((NODES_PER_FACE * FACES_PER_ELEMENT, self.mesh.number_of_elements), order='F')
        dEx = dEx.reshape((NODES_PER_FACE * FACES_PER_ELEMENT, self.mesh.number_of_elements), order='F')
        dEy = dEy.reshape((NODES_PER_FACE * FACES_PER_ELEMENT, self.mesh.number_of_elements), order='F')
        dEz = dEz.reshape((NODES_PER_FACE * FACES_PER_ELEMENT, self.mesh.number_of_elements), order='F')

        self.Ex = self.Ex.reshape(NODES_PER_ELEMENT, self.mesh.number_of_elements, order='F')
        self.Ey = self.Ey.reshape(NODES_PER_ELEMENT, self.mesh.number_of_elements, order='F')
        self.Ez = self.Ez.reshape(NODES_PER_ELEMENT, self.mesh.number_of_elements, order='F')

        alpha = 1  # => full upwinding

        normal_dot_dH = nx * dHx + ny * dHy + nz * dHz
        normal_dot_dE = nx * dEx + ny * dEy + nz * dEz

        fluxHx = -ny * dEz + nz * dEy + alpha * (dHx - normal_dot_dH * nx)
        fluxHy = -nz * dEx + nx * dEz + alpha * (dHy - normal_dot_dH * ny)
        fluxHz = -nx * dEy + ny * dEx + alpha * (dHz - normal_dot_dH * nz)

        fluxEx = ny * dHz - nz * dHy + alpha * (dEx - normal_dot_dE * nx)
        fluxEy = nz * dHx - nx * dHz + alpha * (dEy - normal_dot_dE * ny)
        fluxEz = nx * dHy - ny * dHx + alpha * (dEz - normal_dot_dE * nz)

        # evaluate local spatial derivatives
        curlHx, curlHy, curlHz = self.curl(self.Hx, self.Hy, self.Hz)
        curlEx, curlEy, curlEz = self.curl(self.Ex, self.Ey, self.Ez)

        # calculate Maxwell's right hand side
        rhsHx = -curlEx + self.element_operators.lift @ (self.tetrahedron_maps.face_scale * fluxHx / 2)
        rhsHy = -curlEy + self.element_operators.lift @ (self.tetrahedron_maps.face_scale * fluxHy / 2)
        rhsHz = -curlEz + self.element_operators.lift @ (self.tetrahedron_maps.face_scale * fluxHz / 2)

        rhsEx = curlHx + self.element_operators.lift @ (self.tetrahedron_maps.face_scale * fluxEx / 2)
        rhsEy = curlHy + self.element_operators.lift @ (self.tetrahedron_maps.face_scale * fluxEy / 2)
        rhsEz = curlHz + self.element_operators.lift @ (self.tetrahedron_maps.face_scale * fluxEz / 2)

        return rhsHx, rhsHy, rhsHz, rhsEx, rhsEy, rhsEz

    def run(self):
        self.set_time_step_size()
        self.set_initial_conditions()
        while self.time < self.final_time:
            self.increment_time()

        plot_total_energy(self.total_energy, self.t, np.max(self.total_energy))

